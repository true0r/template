<!DOCTYPE html>
<html lang="ru">
<head>
    <title>loGo | Главная страница</title>
    <meta charset="utf-8">
    <!--отключить режим совместимости IE-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="stylesheet" href="css/main.css">
    <script src="js/main.js"></script>
    <!--  добавить описане строк ниже  -->
    <!--[if lt IE 9]>
        <script src="libJS/html5shiv.js"></script>
    <![endif]-->
</head>
<body>
    <header>
        <nav class="menuProfile">
            <ul class="clearfix">
                <li><a href="" class="colorLinkL">Вход</a></li>
                <li><span class="devider">|</span></li>
                <li><a href="" class="colorLinkL">Регистрация</a></li>
                <li><a href=""><i class="fa fa-shopping-cart fa-fw"></i></a></li>
                <li><a href=""><i class="fa fa-heart fa-fw"></i></a></li>
                <li><a href=""><i class="fa fa-eye fa-fw"></i></a></li>
            </ul>
        </nav>
        <div class="logo">
           <hgroup>
                <h1>loGo</h1>
                <h2>Улыбнись и мир твой</h2>
            </hgroup>
        </div>
        <nav class="menu-main">
            <ul  class="clearfix">
                <li><a href="">Главная</a> </li>
                <li><a href="">Услуги</a> </li>
                <li><a href="">Работы</a> </li>
                <li><a href="">О себе</a> </li>
            </ul>
        </nav>
    </header>
    <article></article>
    <footer>
        <address class="vcard">&copy; 2014 <span class="fn">Галайдюк Александр</span>, по всем вопосам пишите по одресу <a class="email" href="mailto:galaydyukav@gmail.com">galaydyukav@gmail.com</a></address>
    </footer>
</body>
</html>
